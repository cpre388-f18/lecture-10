package edu.iastate.jmay.lecture10_handler;

import android.os.Handler;
import android.os.HandlerThread;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.Locale;
import java.util.concurrent.atomic.AtomicInteger;

public class MainActivity extends AppCompatActivity {
    /** The one TextView on the layout. */
    private TextView myTextView;
    /**
     * This is a thread-safe integer.  It must be thread-safe because the main thread and an
     * instance of MyEventLoop are both operating on it.
     */
    private AtomicInteger i;
    /**
     * The object that creates and controls the second event loop thread (in parallel to the main
     * thread).
     */
    private HandlerThread eventLoopThreadController = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        myTextView = findViewById(R.id.myTextView);
        // Initialize the thread-safe int to 0.
        i = new AtomicInteger(0);
        // Construct an instance of the event loop handler thread.  This does not start the thread.
//        eventLoopThreadController = new MyEventLoop();
    }

    /**
     * Click event handler for the start button.
     * @param v the calling View (the start button)
     */
    public void onStartButtonClick(View v) {
        // Only allow one background event handler thread.
        if (eventLoopThreadController == null) {
            // Prepare the HandlerThread to create a thread with an event loop.
            eventLoopThreadController = new HandlerThread("MyEventLoopThread");
            // Launch a new thread with an event handler loop.
            eventLoopThreadController.start();
            // Set up a Handler that operates on the new thread's EventQueue, so events can be
            // posted into the EventQueue.
            Handler h = new Handler(eventLoopThreadController.getLooper());
            // Post an event immediately into the event queue to start the intermittent worker.
            h.post(new LoopRunnable());
        }
    }

    /**
     * Event handler for the stop button.
     * @param v the calling View (the stop button)
     */
    public void onStopButtonClick(View v) {
        if (eventLoopThreadController != null) {
            // This quits the secondary event loop.
            eventLoopThreadController.quit();
            eventLoopThreadController = null;
        }
    }

    /**
     * The class to control the event handler loop (Looper) on a new thread.  You may extend the
     * class like below for more control.  Or you simply use the default implementation.
     */
//    private class MyEventLoop extends HandlerThread {
//        private Handler mHandler;
//
//        public MyEventLoop() {
//            // HandlerThread needs a name for the thread.
//            super("MyEventLoop");
//        }
//
//        @Override
//        protected void onLooperPrepared() {
//            super.onLooperPrepared();
//            mHandler = new Handler(getLooper());
//            mHandler.post(new LoopRunnable());
//        }
//    }

    /**
     * This is the Runnable that runs intermittently on the event handler thread started by
     * eventLoopThreadController.  It runs because it was started on line 48 and continues because
     * line 93.
     */
    private class LoopRunnable implements Runnable {
        @Override
        public void run() {
            // Do work.  This could be a long running operation.
            i.incrementAndGet();
            // Post an event to the UI thread to update.  This is sending an event across threads.
            MainActivity.this.runOnUiThread(new UpdateUIRunnable());
            // Post a delayed event to the MyEventLoop thread to continue the LoopRunnable running.
            new Handler().postDelayed(LoopRunnable.this, 1000);
        }
    }

    /**
     * This is a runnable that is used for updating the UI indirectly from a different thread.
     */
    private class UpdateUIRunnable implements Runnable {
        @Override
        public void run() {
            updateUI();
        }
    }

    /**
     * This is a helper function for updating the UI.  This must be called on the main thread.
     */
    private void updateUI() {
        myTextView.setText(String.format(Locale.getDefault(), "%d", i.get()));
    }
}
